# sedregex

[![pipeline status](https://gitlab.com/mexus/sedregex/badges/master/pipeline.svg)](https://gitlab.com/mexus/sedregex/commits/master)
[![crates.io](https://img.shields.io/crates/v/sedregex.svg)](https://crates.io/crates/sedregex)
[![docs.rs](https://docs.rs/sedregex/badge.svg)](https://docs.rs/sedregex)

[[Release docs]](https://docs.rs/sedregex/)

[[Master docs]](https://mexus.gitlab.io/sedregex/sedregex/)

A simple sed-like library that uses regex under the hood.

### Usage

There are basically two public interfaces which you can enjoy:

* [`find_and_replace`], which is useful for one-time processing of a bunch of commands,
* and [`ReplaceCommand`] when you need to run the same command multiple times.

### Examples

Let's jump straight to the examples!

```rust
use sedregex::{find_and_replace, ReplaceCommand};
fn main() {
    // Both case-insensitive and global:
    assert_eq!(
        "Please stop wuts oh wut.",
        ReplaceCommand::new("s/lol/wut/ig").unwrap().execute("Please stop Lols oh lol."),
    );

    // Multiple commands in one go:
    assert_eq!(
        "Please stop nos oh no.",
        find_and_replace("Please stop Lols oh lol.", &["s/lol/wut/ig", "s/wut/no/g"]).unwrap()
    );

    // Same, but skipping the `s` character.
    assert_eq!(
        "Please stop wuts oh wut.",
        find_and_replace("Please stop Lols oh lol.", &["/lol/wut/ig"]).unwrap()
    );

    // Skipping the flags.
    assert_eq!(
        "Please stop Lols oh wut.",
        find_and_replace("Please stop Lols oh lol.", &["/lol/wut/"]).unwrap()
    );

    // Skipping the last slash.
    assert_eq!(
        "Please stop Lols oh wut.",
        find_and_replace("Please stop Lols oh lol.", &["/lol/wut"]).unwrap()
    );

    // Escaping a slash in a replace part.
    assert_eq!(
        r"Please stop wut/s oh wut/.",
        find_and_replace("Please stop Lols oh lol.", &[r"s/lol/wut\//gi"]).unwrap()
    );

    // Some regex stuff.
    // Also note the lack of the trailing slash: it's opitonal!
    assert_eq!(
        "Second, First",
        find_and_replace(
            "First, Second",
            &[r"s/(?P<first>[^,\s]+),\s+(?P<last>\S+)/$last, $first"],
        ).unwrap()
    );

    // Ok let's go with some simple regex stuff.
    assert_eq!(
        "Some weird typo",
        find_and_replace("Some wierd typo", &[r"s/ie/ei/"]).unwrap()
    );
}
```

### License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

#### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
